﻿using BrightIdeasSoftware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastRegistryConfig.View
{
    class VComboBox : VItem<Model.MComboBox>
    {
        public VComboBox(TreeListView tree, Model.MComboBox comboBox) : base(tree, comboBox) { }

        override protected void OnEditStart(CellEditEventArgs e)
        {
            var editor = new ComboBox();
            editor.Bounds = e.CellBounds;
            editor.DropDownStyle = ComboBoxStyle.DropDownList;

            var cbModel = (Model.MComboBox)m_entry;

            foreach(var option in cbModel.Options)
            {
                editor.Items.Add(option.Value);
            }

            editor.SelectedIndex = editor.FindString(cbModel.Options[(string)e.Value]);

            e.Control = editor;
        }

        protected override void OnConvert(CellEditEventArgs e)
        {
            var cbModel = (Model.MComboBox)m_entry;
            var editor = (ComboBox)e.Control;

            e.NewValue = cbModel.Options.ElementAt(editor.SelectedIndex).Key;
        }
    }
}
