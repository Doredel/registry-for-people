﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastRegistryConfig.Model;
using BrightIdeasSoftware;
using System.Windows.Forms;

namespace FastRegistryConfig.View
{
    class VNumber : VItem<Model.MNumber>
    {
        public VNumber(TreeListView tree, Model.MNumber item) : base(tree, item){}

        override protected void OnEditStart(CellEditEventArgs e)
        { 
            var editor = new NumericUpDown();
            editor.Bounds = e.CellBounds;
            decimal value = 0;
            editor.Maximum = Int32.MaxValue;
            editor.DecimalPlaces = 0;
            editor.Value = decimal.TryParse((string)e.Value, out value) ? value : 0;
            e.Control = editor;
        }

        protected override void OnConvert(CellEditEventArgs e)
        {
            e.NewValue = (Math.Floor((decimal)e.NewValue)).ToString();
        }
    }
}
