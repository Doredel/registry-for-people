﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastRegistryConfig.Model;
using BrightIdeasSoftware;
using System.Windows.Forms;

namespace FastRegistryConfig.View
{
    class VCheckbox : VItem<Model.MCheckbox>
    {
        public VCheckbox(TreeListView tree, MCheckbox item) : base(tree, item){}

        override protected void OnEditStart(CellEditEventArgs e)
        {
            var editor = new CheckBox();
            editor.Bounds = e.CellBounds;
            editor.Checked = e.Value.Equals(m_item.TrueValue);
            e.Control = editor;
        }

        protected override void OnConvert(CellEditEventArgs e)
        {
            e.NewValue = (bool)e.NewValue ? m_item.TrueValue : m_item.FalseValue;
        }
    }
}
