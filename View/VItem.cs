﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightIdeasSoftware;

namespace FastRegistryConfig.View
{
    class VItem<T> : VEntry where T : Model.MItem
    {
        protected readonly T m_item;

        public VItem(TreeListView tree, T item) : base(tree, item)
        {
            m_item = item;
            Value = item.Value;
        }

        public override void UpdateValue() => Value = m_item.Value;

        protected override void OnFormatValue(FormatCellEventArgs e)
        {
            if(!m_item.Exists)
                e.SubItem.ForeColor = System.Drawing.Color.FromArgb(128, 128, 128);
            else
                e.SubItem.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
        }
    }
}
