﻿using BrightIdeasSoftware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastRegistryConfig.View
{
    abstract class VEntry
    {
        protected readonly Model.MEntry m_entry;
        protected readonly TreeListView m_tree;

        public string Name => m_entry.Name;
        public string Description => m_entry.Description;
        virtual public bool Expandable => false;

        public VEntry(TreeListView tree, Model.MEntry entry)
        {
            m_tree = tree;
            m_entry = entry;
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        private string _value;

        public void HandleEditStarting(object sender, CellEditEventArgs e) => OnEditStart(e);

        public void HandleEditFinishing(object sender, CellEditEventArgs e)
        {
            if (e.Cancel)
                return;

            OnConvert(e);

            if (e.NewValue == e.Value)
                return;

            OnEditFinish(e);
            try
            {
                m_entry.OnChange?.Invoke((string)e.NewValue);
            }
            catch(Exception ex)
            {
                Program.Alert(ex.Message);
            }
        }

        public void HandleFormatCell(object sender, FormatCellEventArgs e)
        {
            switch(e.Column.AspectName)
            {
                case "Name":
                    OnFormatName(e);
                    break;
                case "Value":
                    OnFormatValue(e);
                    break;
            }
        }

        virtual public void HandleContextMenu(object evtSender, CellRightClickEventArgs e)
        {
            if (e.Column.AspectName != "Name")
                return;

            var menu = new ContextMenuStrip();

            if (!(e.Model is VListOption))
                menu.Items.Add("Reset", null, OnReset);

            if(e.Model is VGroup && ((VGroup)e.Model).Mutable)
                menu.Items.Add("Remove", null, OnRemove);

            e.MenuStrip = menu;
        }

        protected void OnReset(object sender, EventArgs e)
        {
            m_entry.Reset();
            UpdateValue();
            m_tree.Update();
        }

        protected void OnRemove(object sender, EventArgs e)
        {
            ((Model.MGroup)m_entry).Remove();
            m_tree.Update();
        }

        virtual protected void OnEditStart(CellEditEventArgs e) => e.Cancel = true;
        virtual protected void OnEditFinish(CellEditEventArgs e) { }
        virtual protected void OnConvert(CellEditEventArgs e) { }
        virtual protected void OnFormatName(FormatCellEventArgs e) { }
        virtual protected void OnFormatValue(FormatCellEventArgs e) { }
        virtual public void UpdateValue() { }
    }
}
