﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastRegistryConfig.Model;
using BrightIdeasSoftware;

namespace FastRegistryConfig.View
{
    class VList : VItem<Model.MList>
    {
        public List<VListOption> Options { get; private set; }

        override public bool Expandable => Options.Count > 0;

        public VList(TreeListView tree, MList list) : base(tree, list)
        {
            Options = new List<VListOption>();

            foreach(var option in list.Options)
                Options.Add(new VListOption(tree, this, option));
        }

        public void OnChange()
        {
            var listModel = (MList)m_entry;
            var newValue = listModel.GetOptionsValue();
            Value = newValue;
            listModel.OnChange(Value);
        }

        public override void UpdateValue()
        {
            base.UpdateValue();

            foreach (var option in Options)
                option.UpdateValue();
        }
    }
}
