﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastRegistryConfig.Model;
using BrightIdeasSoftware;
using System.Windows.Forms;

namespace FastRegistryConfig.View
{
    class VListOption : VEntry
    {
        readonly bool m_valid;

        private readonly VList m_parent;
        private readonly MListOption m_option;

        public VListOption(TreeListView tree, VList parent, MListOption option) : base(tree, option)
        {
            m_parent = parent;
            m_option = option;
            m_valid = option.Valid;
            Value = option.Enabled ? "on" : "off";
        }

        override protected void OnEditStart(CellEditEventArgs e)
        {
            if (!m_valid)
            {
                e.Cancel = true;
                return;
            }

            var editor = new CheckBox();
            editor.Bounds = e.CellBounds;
            editor.Checked = e.Value.Equals("on");
            e.Control = editor;
        }

        protected override void OnFormatName(FormatCellEventArgs e)
        {
            if(m_option.Temporary)
                e.SubItem.ForeColor = System.Drawing.Color.FromArgb(128, 128, 128);
            else
                e.SubItem.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
        }

        protected override void OnConvert(CellEditEventArgs e)
        {
            e.NewValue = (bool)e.NewValue ? "on" : "off";
        }

        protected override void OnEditFinish(CellEditEventArgs e)
        {
            ((MListOption)m_entry).Set((string)e.NewValue == "on");

            try
            {
                m_parent.OnChange();
            }
            catch(Exception ex)
            {
                Program.Alert(ex.Message);
            }
            
        }

        public override void UpdateValue() => Value = m_option.Enabled ? "on" : "off";
    }
}
