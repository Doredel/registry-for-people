﻿using BrightIdeasSoftware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastRegistryConfig.View
{
    class VGroup : VEntry
    {
        public readonly List<VEntry> Children;

        private readonly Model.MGroup m_group;
        override public bool Expandable => Children.Count > 0;
        public readonly bool Mutable;

        public VGroup(TreeListView tree, Model.MGroup group) : base(tree, group)
        {
            m_group = group;
            Mutable = m_group.Mutable;

            Children = new List<VEntry>();

            foreach (var entry in m_group.Contents)
            {
                if (entry is Model.MGroup)
                    Children.Add(new VGroup(tree, entry as Model.MGroup));
                else if(entry is Model.MNumber)
                    Children.Add(new VNumber(tree, entry as Model.MNumber));
                else if (entry is Model.MCheckbox)
                    Children.Add(new VCheckbox(tree, entry as Model.MCheckbox));
                else if (entry is Model.MList)
                    Children.Add(new VList(tree, entry as Model.MList));
                else if (entry is Model.MComboBox)
                    Children.Add(new VComboBox(tree, entry as Model.MComboBox));
                else
                    Children.Add(new VItem<Model.MItem>(tree, entry as Model.MItem));
            }
        }

        public override void UpdateValue()
        {
            foreach (var entry in Children)
                entry.UpdateValue();
        }
    }
}
