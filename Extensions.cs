﻿using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastRegistryConfig
{
    static class Extensions
    {
        public static string TryGetValue(this XmlNode xNode, string name, string defaultValue = "")
        {
            if (xNode.Attributes == null)
                return defaultValue;

            var attrVal = xNode.Attributes[name];

            return attrVal == null ? defaultValue : attrVal.Value;
        }
            

        public static bool HasValue(this XmlNode xNode, string name)
        {
            if (xNode.Attributes == null)
                return false;

            return xNode.Attributes[name] != null;
        }
    }
}
