﻿namespace FastRegistryConfig
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.registryTree = new BrightIdeasSoftware.TreeListView();
            this.column1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.column2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.entryMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.menuItemFile = new System.Windows.Forms.MenuItem();
            this.menuItemOpenConfig = new System.Windows.Forms.MenuItem();
            this.menuItemReloadConfig = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.registryTree)).BeginInit();
            this.entryMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // registryTree
            // 
            this.registryTree.AllColumns.Add(this.column1);
            this.registryTree.AllColumns.Add(this.column2);
            this.registryTree.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.registryTree.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.SingleClick;
            this.registryTree.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.column1,
            this.column2});
            this.registryTree.Location = new System.Drawing.Point(-1, 0);
            this.registryTree.Margin = new System.Windows.Forms.Padding(0);
            this.registryTree.MultiSelect = false;
            this.registryTree.Name = "registryTree";
            this.registryTree.OwnerDraw = true;
            this.registryTree.ShowGroups = false;
            this.registryTree.ShowItemToolTips = true;
            this.registryTree.Size = new System.Drawing.Size(550, 545);
            this.registryTree.TabIndex = 0;
            this.registryTree.UseCellFormatEvents = true;
            this.registryTree.UseCompatibleStateImageBehavior = false;
            this.registryTree.UseExplorerTheme = true;
            this.registryTree.View = System.Windows.Forms.View.Details;
            this.registryTree.VirtualMode = true;
            // 
            // column1
            // 
            this.column1.AspectName = "Name";
            this.column1.CellPadding = null;
            this.column1.FillsFreeSpace = true;
            this.column1.IsEditable = false;
            this.column1.Text = "Name";
            this.column1.ToolTipText = "";
            this.column1.Width = 255;
            // 
            // column2
            // 
            this.column2.AspectName = "Value";
            this.column2.CellPadding = null;
            this.column2.Text = "Value";
            this.column2.ToolTipText = "";
            this.column2.Width = 255;
            // 
            // entryMenu
            // 
            this.entryMenu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.entryMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetToolStripMenuItem});
            this.entryMenu.Name = "entryMenu";
            this.entryMenu.Size = new System.Drawing.Size(148, 40);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(147, 36);
            this.resetToolStripMenuItem.Text = "Reset";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile});
            // 
            // menuItemFile
            // 
            this.menuItemFile.Index = 0;
            this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemOpenConfig,
            this.menuItemReloadConfig});
            this.menuItemFile.Text = "File";
            // 
            // menuItemOpenConfig
            // 
            this.menuItemOpenConfig.Index = 0;
            this.menuItemOpenConfig.Shortcut = System.Windows.Forms.Shortcut.F4;
            this.menuItemOpenConfig.Text = "Open config";
            this.menuItemOpenConfig.Click += new System.EventHandler(this.OnOpenConfig);
            // 
            // menuItemReloadConfig
            // 
            this.menuItemReloadConfig.Index = 1;
            this.menuItemReloadConfig.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.menuItemReloadConfig.Text = "Reload config";
            this.menuItemReloadConfig.Click += new System.EventHandler(this.OnReloadConfig);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(548, 543);
            this.Controls.Add(this.registryTree);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Menu = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Registry for People";
            this.Load += new System.EventHandler(this.OnFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.registryTree)).EndInit();
            this.entryMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView registryTree;
        private BrightIdeasSoftware.OLVColumn column1;
        private BrightIdeasSoftware.OLVColumn column2;
        private System.Windows.Forms.ContextMenuStrip entryMenu;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.MenuItem menuItemFile;
        private System.Windows.Forms.MenuItem menuItemOpenConfig;
        private System.Windows.Forms.MenuItem menuItemReloadConfig;
    }
}

