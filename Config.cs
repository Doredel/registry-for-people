﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastRegistryConfig
{
    class Config
    {
        readonly string m_filename;
        public Model.MGroup Root { get; private set; }

        public List<string> Errors { get; private set; }

        public Config(string filename)
        {
            Errors = new List<string>();
            m_filename = filename;
        }

        public void Load(Action completeCallback)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(m_filename);
                XmlElement xRoot = xDoc.DocumentElement;
                Root = new Model.MConfigGroup("", xRoot);
            }
            catch(Exception ex)
            {
                AddError(ex.Message);
            }
            
            completeCallback();
        }

        public void AddError(string text) => Errors.Add(text);
    }
}
