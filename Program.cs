﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastRegistryConfig
{
    static class Program
    {
        internal static string ConfigPath = Application.StartupPath + @"\Config.xml";
        internal static Config Config;
        internal static MainForm Form;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form = new MainForm();
            LoadConfig();
            Application.Run(Form);
        }

        public static void LoadConfig()
        {
            Config = new Config(ConfigPath);
            Task.Run(() => Config.Load(Form.OnConfigLoadFinished));
        }

        public static void Alert(string message)
        {
            MessageBox.Show(message, "Config errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
