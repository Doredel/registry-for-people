﻿using BrightIdeasSoftware;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastRegistryConfig
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            registryTree.CanExpandGetter = (object entry) => ((View.VEntry)entry).Expandable;
            registryTree.ChildrenGetter = (object entry) =>
            {
                if (entry is View.VGroup)
                    return ((View.VGroup)entry).Children;
                else if (entry is View.VList)
                    return ((View.VList)entry).Options;
                return null;
            };

            registryTree.CellToolTipGetter = (OLVColumn column, object entry) => ((View.VEntry)entry).Description;
            registryTree.CellEditStarting += (object evtSender, CellEditEventArgs evtArgs) => ((View.VEntry)evtArgs.RowObject).HandleEditStarting(evtSender, evtArgs);
            registryTree.CellEditFinishing += (object evtSender, CellEditEventArgs evtArgs) => ((View.VEntry)evtArgs.RowObject).HandleEditFinishing(evtSender, evtArgs);
            registryTree.FormatCell += (object evtSender, FormatCellEventArgs evtArgs) => ((View.VEntry)evtArgs.Model).HandleFormatCell(evtSender, evtArgs);
            registryTree.CellRightClick += (object evtSender, CellRightClickEventArgs evtArgs) =>
            {
                if (evtArgs.Model == null)
                    return;
                ((View.VEntry)evtArgs.Model).HandleContextMenu(evtSender, evtArgs);
            };
        }

        public void OnConfigLoadFinished()
        {
            Invoke(new Action(InitializeTree));
            Invoke(new Action(ShowErrors));
            return;
        }

        public void ShowErrors()
        {
            if (Program.Config.Errors.Count == 0)
                return;

            String msg = "";
            int msgsCount = 0;
            foreach(var error in Program.Config.Errors)
            {
                ++msgsCount;
                msg += error + "\n";
                if (msgsCount == 10)
                    break;
            }

            if(Program.Config.Errors.Count > 10)
                msg += $"{Program.Config.Errors.Count - 10} more errors...";

            Program.Alert(msg);
        }

        private void InitializeTree()
        {
            if (Program.Config?.Root == null)
                return;

            var root = new View.VGroup(registryTree, Program.Config.Root);
            registryTree.SetObjects(root.Children);
            registryTree.ExpandAll();
        }

        private void OnOpenConfig(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Program.ConfigPath);
        }

        private void OnReloadConfig(object sender, EventArgs e)
        {
            Program.LoadConfig();
        }
    }
}
