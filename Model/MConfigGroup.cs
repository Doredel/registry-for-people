﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FastRegistryConfig.Model
{
    class MConfigGroup : MGroup
    {
        public MConfigGroup(string parentKey, XmlNode xNode) : base(parentKey, xNode)
        {
            foreach (XmlNode child in xNode)
            {
                if (child.NodeType == XmlNodeType.Comment)
                    continue;

                switch (child.Name)
                {
                    case "Item":
                        Contents.Add(MItem.Create(RegistryKeyPath, child));
                        break;
                    case "Group":
                        Contents.Add(MGroup.Create(RegistryKeyPath, child));
                        break;
                    default:
                        Program.Config.AddError($"Unknown node type: '{child.Name}'");
                        break;
                }
            }
        }
    }
}
