﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastRegistryConfig.Model
{
    class MNumber : ItemGeneric<int>
    {
        public MNumber(string parentPath, XmlNode xNode) : base(parentPath, xNode)
        {
        }

        protected override void Set(string value)
        {
            int intValue = 0;
            Store(Int32.TryParse(value, out intValue) ? intValue : 0);
        }

        protected override void Store(int newValue)
        {
            CheckAndSetRegistry(newValue.ToString());
        }
    }
}
