﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastRegistryConfig.Model
{
    abstract class MEntry
    {
        public Action<string> OnChange;

        public string Name { get; protected set; }
        public readonly string Description;
        protected string RegistryKeyPath;

        public MEntry(string parentKey, string name, string description)
        {
            Name = name;
            Description = description;
            RegistryKeyPath = parentKey;
        }

        public MEntry(string parentKey, XmlNode xNode) : 
            this(
                parentKey, 
                xNode.TryGetValue("name", xNode.TryGetValue("param", "???")), 
                xNode.TryGetValue("desc"))
        { }

        abstract public void Reset();
        abstract public void Refresh();
    }
}
