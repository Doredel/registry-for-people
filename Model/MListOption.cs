﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FastRegistryConfig.Model
{
    class MListOption : MEntry
    {
        public readonly bool Valid;
        public string Value { get; protected set; }
        public bool Enabled { get; private set; }
        public readonly bool Temporary;

        private MList m_parent;

        private MListOption(MList parent, string name, string value, string description, bool enabled, bool valid, bool fromConf) : base("", name, description)
        {
            m_parent = parent;
            Name = name;
            Value = value;
            Enabled = enabled;
            Valid = valid;
            Temporary = !fromConf;
        }

        public void Set(bool value)
        {
            Enabled = value;
        }

        public override void Refresh()
        {

        }

        public override void Reset() => Enabled = false;

        public static MListOption CreateTemporary(MList parent, string name) => new MListOption(parent, name, name, "", true, true, false);

        public static MListOption Create(MList parent, XmlNode xNode, List<string> listEnabledOptions)
        {
            bool isValid = true;
            string value = "";

            if (xNode.HasValue("value"))
                value = xNode.TryGetValue("value");
            else
                isValid = false;

            var name = xNode.TryGetValue("name", value);

            if (name.Length == 0)
                name = "???";

            var description = xNode.TryGetValue("desc");

            bool enabled = isValid ? listEnabledOptions.Contains(value) : false;
            
            return new MListOption(parent, name, value, description, enabled, isValid, true);
        }
    }
}
