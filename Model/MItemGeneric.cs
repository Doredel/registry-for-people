﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastRegistryConfig.Model
{
    abstract class ItemGeneric<T> : MItem
    {
        protected ItemGeneric(string parentPath, XmlNode xNode) : base(parentPath, xNode, typeof(T) == typeof(string) ? "" : default(T).ToString()) { }

        abstract protected void Store(T newValue);
    }
}
