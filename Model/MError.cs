﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FastRegistryConfig.Model
{
    class MError : ItemGeneric<string>
    {
        public MError(XmlNode xNode) : base("", xNode)
        {
            if(!xNode.HasValue("kind"))
                Program.Config.AddError($"Missing param kind for item '{Name}'");
        }

        protected override void Set(string value){}

        override protected void Store(string newValue){}
    }
}
