﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace FastRegistryConfig.Model
{
    abstract class MItem : MEntry
    {
        public Action<string> OnCreate;
        public Action OnChangeDetected;

        private string m_paramName = "";
        private readonly string m_default;
        public string Value { get; private set; }
        public bool Exists { get; private set; }
        private readonly RegistryKey m_key;

        protected MItem(string parentKey, XmlNode xNode, string defaultDefault) : base(parentKey, xNode)
        {
            if (xNode.HasValue("param"))
                m_paramName = xNode.TryGetValue("param");
            else
                Program.Config.AddError($"Missing param name for item '{Name}'");

            m_default = xNode.TryGetValue("default", defaultDefault);

            m_key = Registry.LocalMachine.OpenSubKey(parentKey, RegistryKeyPermissionCheck.ReadWriteSubTree);

            LoadRegistry();
            OnChange += Set;
            OnCreate += CreateRegistryValue;
        }

        abstract protected void Set(string value);

        public override void Reset() => ResetRegistry();
        public override void Refresh() => LoadRegistry();

        protected string LoadRegistry()
        {
            if (m_paramName.Length == 0 || m_key == null)
            {
                Value = m_default;
                Exists = false;
            }
            else
            {
                try
                {
                    m_key.GetValueKind(m_paramName);
                    Exists = true;
                }
                catch(System.IO.IOException)
                {
                    Exists = false;
                }

                if (!Exists)
                    Value = m_default;
                else
                    Value = (string)m_key.GetValue(m_paramName, m_default);
            }

            return Value;
        }

        protected void ResetRegistry()
        {
            if (!Exists)
                return;

            m_key.DeleteValue(m_paramName);
            Exists = false;
            Value = m_default;
        }

        protected void CheckAndSetRegistry(string newValue)
        {
            var oldValue = Value;

            if (oldValue == newValue)
                return;

            var regValue = LoadRegistry();

            if(!Exists)
            {
                OnCreate?.Invoke(newValue);
                return;
            }

            if(oldValue != regValue)
            {
                OnChangeDetected?.Invoke();
                return;
            }

            SetRegistry(newValue);
        }

        private void CreateRegistryValue(string value)
        {
            m_key.SetValue(m_paramName, value);
            Exists = true;
            Value = value;
        }

        private void SetRegistry(string newValue)
        {
            m_key.SetValue(m_paramName, newValue);
            Value = newValue;
        }

        public static MItem Create(string parentKey, XmlNode xNode)
        {
            string kind;

            kind = xNode.TryGetValue("kind", "string");

            switch (kind)
            {
                case "number":
                    return new MNumber(parentKey, xNode);
                case "string":
                    return new MString(parentKey, xNode);
                case "list":
                    return new MList(parentKey, xNode);
                case "checkbox":
                    return new MCheckbox(parentKey, xNode);
                case "combobox":
                    return new MComboBox(parentKey, xNode);
                default:
                    Program.Config.AddError($"Unknown config item kind: '{kind}'");
                    return new MError(xNode);
            }
        }
    }
}
