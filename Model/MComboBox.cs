﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FastRegistryConfig.Model
{
    class MComboBox : ItemGeneric<string>
    {
        public Dictionary<string, string> Options { get; private set; }

        public MComboBox(string parentPath, XmlNode xNode) : base(parentPath, xNode)
        {
            Options = new Dictionary<string, string>();

            foreach (XmlNode child in xNode)
            {
                if (child.NodeType == XmlNodeType.Comment)
                    continue;

                if (child.Name != "Option")
                {
                    Program.Config.AddError($"Unknown node type: '{child.Name}' in combobox '{Name}'");
                    continue;
                }

                if (!child.HasValue("value"))
                {
                    Program.Config.AddError($"Missing value of option for combobox '{Name}'");
                    continue;
                }

                var value = child.TryGetValue("value");
                var name = child.TryGetValue("name");

                if (name.Length > 0)
                    Options.Add(value, $"{(String.IsNullOrWhiteSpace(value) ? "empty" : value)} - {name}");
                else
                    Options.Add(value, value);
            }

            if (!Options.ContainsKey(Value))
                Options.Add(Value, $"{(String.IsNullOrWhiteSpace(Value) ? "empty" : Value)} - Unknown option");
        }

        protected override void Set(string value)
        {
            Store(value);
        }

        override protected void Store(string value)
        {
            CheckAndSetRegistry(value);
        }
    }
}
