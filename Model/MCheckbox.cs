﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FastRegistryConfig.Model
{
    class MCheckbox : ItemGeneric<bool>
    {
        public String TrueValue { private set; get; }
        public String FalseValue { private set; get; }

        public MCheckbox(string parentPath, XmlNode xNode) : base(parentPath, xNode)
        {
            if (!xNode.HasChildNodes)
            {
                TrueValue = "1";
                FalseValue = "0";
                return;
            }
   

            var trueVals = xNode.SelectNodes("True");
            var falseVals = xNode.SelectNodes("False");

            if(trueVals.Count > 1)
                Program.Config.AddError($"Multiple true values for checkbox '{Name}'");
            if (falseVals.Count > 1)
                Program.Config.AddError($"Multiple false values for checkbox '{Name}'");

            TrueValue = trueVals.Count > 0 ? trueVals[0].TryGetValue("value", "1") : "1";
            FalseValue = falseVals.Count > 0 ? falseVals[0].TryGetValue("value", "0") : "0";

            if(TrueValue.Equals(FalseValue))
                Program.Config.AddError($"True and false values are equal '{TrueValue}' for checkbox '{Name}'");
        }

        protected override void Set(string value)
        {
            Store(value == TrueValue);
        }

        protected override void Store(bool newValue)
        {
            CheckAndSetRegistry(newValue ? TrueValue : FalseValue);
        }
    }
}
