﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace FastRegistryConfig.Model
{
    class MList : MString
    {
        public List<MListOption> Options { get; private set; }

        public MList(string parentPath, XmlNode xNode) : base(parentPath, xNode)
        {
            Options = new List<MListOption>();

            var tempValue = new StringBuilder();

            foreach(var c in Value)
            {
                if (Char.IsWhiteSpace(c))
                    continue;
                tempValue.Append(c, 1);
            }

            var valueEntries = new List<string>();

            if (tempValue.Length > 0)
                valueEntries = tempValue.ToString().ToLower().Split(',').ToList();

            foreach (XmlNode child in xNode)
            {
                if (child.NodeType == XmlNodeType.Comment)
                    continue;

                if (child.Name != "Option")
                {
                    Program.Config.AddError($"Unknown node type: '{child.Name}' in list '{Name}'");
                    continue;
                }

                var option = MListOption.Create(this, child, valueEntries);
                valueEntries.Remove(option.Value.ToLower());
                Options.Add(option);
            }

            foreach(var entry in valueEntries)
                Options.Add(MListOption.CreateTemporary(this, entry));
        }

        public override void Reset()
        {
            foreach (var option in Options)
                option.Reset();

            ResetRegistry();
        }

        public string GetOptionsValue()
        {
            var options = new StringBuilder();

            foreach (var option in Options)
                if(option.Enabled)
                    options.Append(option.Value + ",");
            
            if(options.Length > 0)    
                --options.Length;

            return options.ToString();
        }
    }
}
