﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace FastRegistryConfig.Model
{
    class MKey : MGroup
    {
        public readonly uint Depth;
        public readonly bool Fixed = false;

        public MKey(string parentKey, string name, uint argDepth) : base(parentKey, name)
        {
            Mutable = true;
            Depth = argDepth;
            LoadTree();
        }

        public MKey(string parentKey, XmlNode xNode) : base(parentKey, xNode)
        {
            Fixed = true;
            Mutable = true;
            if (xNode.HasValue("depth"))
            {
                var depthString = xNode.Attributes["depth"].Value;
                if (!UInt32.TryParse(depthString, out Depth))
                {
                    Program.Config.AddError($"Could not parse depth '{depthString}' for '{Name}'");
                    Depth = 0;
                }
            }

            LoadTree();
        }

        public void LoadTree()
        {
            if (Depth == 0)
                return;

            Contents.Clear();
            var key = Registry.LocalMachine.OpenSubKey(RegistryKeyPath, RegistryKeyPermissionCheck.ReadWriteSubTree);

            if (key == null)
                return;

            var subKeys = key.GetSubKeyNames();

            foreach (var subKey in subKeys)
                Contents.Add(new MKey(RegistryKeyPath, subKey, Depth - 1));
        }

        //public void EraseChild()

        override public void Remove()
        {
            var key = Registry.LocalMachine.OpenSubKey(ParentKeyPath, RegistryKeyPermissionCheck.ReadWriteSubTree);
            key.DeleteSubKeyTree(KeyName);

            Refresh();
        }
    }
}
