﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace FastRegistryConfig.Model
{
    class MGroup : MEntry
    {
        public List<MEntry> Contents { get; private set; }

        protected readonly string ParentKeyPath;
        protected readonly string KeyName;
        public bool Mutable { get; protected set; } = false;

        public MGroup(string parentKey, string name) : base(parentKey, name, "")
        {
            Contents = new List<MEntry>();
            KeyName = name;
            ParentKeyPath = RegistryKeyPath;

            if (RegistryKeyPath.Length != 0)
                RegistryKeyPath += '\\' + KeyName;
            else
                RegistryKeyPath = KeyName;
        }

        public MGroup(string parentKey, XmlNode xNode) : base(parentKey, xNode)
        {
            Contents = new List<MEntry>();

            KeyName = xNode.TryGetValue("path");

            if (KeyName.Length > 0 && KeyName.EndsWith("\\"))
                KeyName.Remove(KeyName.Length - 1);

            ParentKeyPath = RegistryKeyPath;

            if (RegistryKeyPath.Length != 0)
                RegistryKeyPath += '\\' + KeyName;
            else
                RegistryKeyPath = KeyName;
        }

        static public MGroup Create(string parentKey, XmlNode xNode)
        {
            if (xNode.HasValue("removable"))
                return new MKey(parentKey, xNode);
            else
                return new MConfigGroup(parentKey, xNode);
        }

        public override void Reset()
        {
            foreach (var entry in Contents)
                entry.Reset();
        }

        public override void Refresh()
        {
            foreach (var entry in Contents)
                entry.Refresh();
        }

        virtual public void Remove() { }
    }
}
