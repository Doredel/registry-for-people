﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FastRegistryConfig.Model
{
    class MString : ItemGeneric<string>
    {
        public MString(string parentPath, XmlNode xNode) : base(parentPath, xNode)
        {
        }

        protected override void Set(string value)
        {
            Store(value);
        }

        override protected void Store(string value)
        {
            CheckAndSetRegistry(value);
        }
    }
}
